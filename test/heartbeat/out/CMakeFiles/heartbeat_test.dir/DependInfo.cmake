# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/src/heartbeat/heartbeat.c" "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/test/heartbeat/out/CMakeFiles/heartbeat_test.dir/C_/Users/tusiek112/Desktop/embedded/tdd_embedded/src/heartbeat/heartbeat.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../src"
  "../../../src/heartbeat"
  "../../catch2/single_include/catch2"
  "../../trompeloeil/include"
  "../../mingw-std-threads"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/test/heartbeat/main.cpp" "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/test/heartbeat/out/CMakeFiles/heartbeat_test.dir/main.cpp.obj"
  "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/test/heartbeat/test.cpp" "C:/Users/tusiek112/Desktop/embedded/tdd_embedded/test/heartbeat/out/CMakeFiles/heartbeat_test.dir/test.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../../src"
  "../../../src/heartbeat"
  "../../catch2/single_include/catch2"
  "../../trompeloeil/include"
  "../../mingw-std-threads"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
