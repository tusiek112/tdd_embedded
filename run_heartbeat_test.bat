cls

@echo off
set mypath=%cd%
@echo %mypath%
set PATH=%PATH%
@echo on

cd %mypath%\test\heartbeat\out\
cmake .. -G "MinGW Makefiles"
mingw32-make

call %mypath%\test\heartbeat\out\heartbeat_test.exe
cd %mypath%
