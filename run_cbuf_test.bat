cls

@echo off
set mypath=%cd%
@echo %mypath%
set PATH=%PATH%
@echo on

cd %mypath%\test\cbuf\out\
cmake .. -G "MinGW Makefiles"
mingw32-make

call %mypath%\test\cbuf\out\cbuf_test.exe
cd %mypath%
